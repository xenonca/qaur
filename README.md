qaur
====

Minetest mod written for the Aurelium server - Start and Finish system for mazes, quests, etc.

Description
-----------

Adds a start and a finish crystal which need to be punched in order to start/finish a maze/quest/etc. Use `/giveup` to give up the current quest. By starting a quest you will be revoked all your privs except for interact and shout. You will get them back as soon as you hit the finish crystal. You can also give up the quest which will teleport you back to the starting crystal.  

License
-------
Code: MIT by xenonca  
Media: MIT by xenonca  

Version
-------
1.0
