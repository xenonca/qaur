local spawnpos = {x=1,y=1,z=1}

minetest.register_node("qaur:start", {
	description = "Start",
	drawtype = "mesh",
	mesh = "point.obj",
	tiles = {"point.png"},
	groups = {not_in_craft_guide = 1, level = 5},
	paramtype = "light",
	use_texture_alpha = true,
	sunlight_propagates = true,
	pointable = true,
	walkable = false,
	light_source = 5,
	selection_box = {
		type = "fixed",
		fixed = {-0.15, -0.25, -0.15, 0.15, 0.25, 0.15}
	},
	on_punch = function(pos, node, player, pointed_thing)
		local name = player:get_player_name()
		if player:get_attribute("started") == nil
			or player:get_attribute("started") == "" then
			local player_privs = minetest.privs_to_string(minetest.get_player_privs(name))
			local player_pos = minetest.pos_to_string(minetest.get_pointed_thing_position(pointed_thing, above))
			player:set_attribute("orig_privs", player_privs)
			player:set_attribute("orig_pos", player_pos)
			minetest.set_player_privs(name, {shout=true, interact=true})
			player:set_attribute("started", "true")
			minetest.chat_send_player(name, minetest.colorize("#fef001", "Start!"))
			minetest.chat_send_player(name, "Give up by typging /giveup")
		else
			minetest.chat_send_player(name, minetest.colorize("#fef001", "You've already started!"))
			minetest.chat_send_player(name, "Give up by typging /giveup")
		end
	end,
})

minetest.register_node("qaur:finish", {
	description = "Finish",
	drawtype = "mesh",
	mesh = "point.obj",
	tiles = {"point.png"},
	groups = {not_in_craft_guide = 1, level = 5},
	paramtype = "light",
	use_texture_alpha = true,
	sunlight_propagates = true,
	pointable = true,
	walkable = false,
	light_source = 5,
	selection_box = {
		type = "fixed",
		fixed = {-0.15, -0.25, -0.15, 0.15, 0.25, 0.15}
	},
	on_punch = function(pos, node, player, pointed_thing)
		local name = player:get_player_name()
		if player:get_attribute("started") == "true" then
			local orig_privs = minetest.string_to_privs(player:get_attribute("orig_privs"))
			minetest.set_player_privs(name, orig_privs)
			player:set_attribute("orig_privs", nil)
			player:set_attribute("started", "")
			minetest.chat_send_player(name, minetest.colorize("#fef001", "Finished!"))
		else
			minetest.chat_send_player(name, minetest.colorize("#fef001", "You haven't started yet!"))
		end
	end,
})

minetest.register_chatcommand("giveup", {
    description = "Give up",
    privs = {interact=true},
	func = function(target)
		local player = minetest.get_player_by_name(target)
		local name = player:get_player_name()
		if player:get_attribute("started") == "true" then
			local orig_privs = minetest.string_to_privs(player:get_attribute("orig_privs"))
			local orig_pos = minetest.string_to_pos(player:get_attribute("orig_pos"))
			minetest.set_player_privs(target, orig_privs)
			player:set_attribute("orig_privs", nil)
			player:set_attribute("started", "")
			player:set_pos(orig_pos)
			minetest.chat_send_player(target, minetest.colorize("#fef001", "You gave up!"))
		else
			minetest.chat_send_player(target, minetest.colorize("#fef001", "You haven't started yet!"))
		end
	end
})